provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "solo-stylist-terraform-backend"
    region = "us-east-1"
    key    = "state"
  }
}