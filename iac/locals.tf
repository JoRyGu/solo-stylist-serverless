locals {
  api_gw_id               = aws_api_gateway_rest_api.api_gateway.id
  api_gw_root_resource_id = aws_api_gateway_rest_api.api_gateway.root_resource_id
  api_gw_exec_arn         = aws_api_gateway_rest_api.api_gateway.execution_arn
}