resource "aws_lambda_function" "api_gateway_authorizer" {
  function_name    = "api_gateway_authorizer"
  handler          = "main"
  filename         = "../artifacts/lambdas/api_authorizer.zip"
  role             = aws_iam_role.authorizer_role.arn
  source_code_hash = filemd5("../artifacts/lambdas/api_authorizer.zip")
  runtime          = "go1.x"
}

resource "aws_iam_role" "authorizer_role" {
  name               = "authorizer_role"
  assume_role_policy = data.aws_iam_policy_document.authorizer_role_policy_document.json
}

resource "aws_iam_role" "invocation_role" {
  name               = "api_gateway_auth_invocation"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.authorizer_role_policy_document.json
}

resource "aws_iam_role_policy" "invocation_policy" {
  name   = "default"
  role   = aws_iam_role.invocation_role.id
  policy = data.aws_iam_policy_document.api_gateway_invocation_policy_document.json
}

resource "aws_api_gateway_authorizer" "authorizer" {
  name                   = "authorizer"
  rest_api_id            = aws_api_gateway_rest_api.api_gateway.id
  authorizer_uri         = aws_lambda_function.api_gateway_authorizer.invoke_arn
  authorizer_credentials = aws_iam_role.invocation_role.arn
}

data "aws_iam_policy_document" "authorizer_role_policy_document" {
  version = "2012-10-17"
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }
    effect = "Allow"
  }
}

data "aws_iam_policy_document" "api_gateway_role_policy_document" {
  version = "2012-10-17"
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      identifiers = ["apigateway.amazonaws.com"]
      type        = "Service"
    }
    effect = "Allow"
  }
}

data "aws_iam_policy_document" "api_gateway_invocation_policy_document" {
  version = "2012-10-17"
  statement {
    actions   = ["lambda:InvokeFunction"]
    resources = [aws_lambda_function.api_gateway_authorizer.arn]
    effect    = "Allow"
  }
}
