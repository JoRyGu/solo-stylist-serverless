resource "aws_iam_role" "lambda_role" {
  name               = "${var.lambda_name}_lambda_role"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role.json
}

resource "aws_iam_policy" "lambda_policy" {
  name        = "${var.lambda_name}_lambda_policy"
  path        = "/"
  description = "AWS IAM policy for Lambda role"
  policy      = data.aws_iam_policy_document.lambda_policy.json
}

resource "aws_iam_role_policy_attachment" "lambda_policy_attach" {
  policy_arn = aws_iam_policy.lambda_policy.arn
  role       = aws_iam_role.lambda_role.name
}
