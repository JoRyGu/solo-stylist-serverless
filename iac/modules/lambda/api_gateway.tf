resource "aws_api_gateway_resource" "lambda_resource" {
  parent_id   = var.parent_resource_id
  path_part   = var.resource_path
  rest_api_id = var.api_gateway_id
}

resource "aws_api_gateway_method" "lambda_method" {
  authorization = var.api_auth
  authorizer_id = var.authorizer_id
  http_method   = var.http_method
  resource_id   = aws_api_gateway_resource.lambda_resource.id
  rest_api_id   = var.api_gateway_id
}

resource "aws_api_gateway_integration" "lambda_integration" {
  http_method             = var.http_method
  integration_http_method = "POST"
  resource_id             = aws_api_gateway_resource.lambda_resource.id
  rest_api_id             = var.api_gateway_id
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda_function.invoke_arn
}

resource "aws_api_gateway_deployment" "lambda_deployment" {
  rest_api_id = var.api_gateway_id
  stage_name  = "dev" #Make this dynamic at some point
  depends_on  = [aws_api_gateway_integration.lambda_integration]
}
