resource "aws_lambda_function" "lambda_function" {
  function_name    = var.lambda_name
  filename         = var.artifact_name
  role             = aws_iam_role.lambda_role.arn
  handler          = var.handler
  runtime          = "go1.x"
  depends_on       = [aws_iam_role_policy_attachment.lambda_policy_attach]
  source_code_hash = filemd5(var.artifact_name)
}

resource "aws_lambda_permission" "api_gateway_invoke_permission" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_function.function_name
  principal     = "apigateway.amazonaws.com"
  statement_id  = "AllowAPIGatewayInvoke"
  source_arn    = "${var.api_gateway_exec_arn}/*/*"
}
