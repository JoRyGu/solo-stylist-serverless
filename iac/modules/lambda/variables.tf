variable "lambda_name" { type = string }
variable "artifact_name" { type = string }
variable "handler" { type = string }
variable "api_gateway_id" { type = string }
variable "parent_resource_id" { type = string }
variable "resource_path" { type = string }
variable "http_method" { type = string }
variable "api_auth" { type = string }
variable "authorizer_id" { type = string }
variable "api_gateway_exec_arn" { type = string }