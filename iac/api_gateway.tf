resource "aws_api_gateway_rest_api" "api_gateway" {
  name = "SoloStylistApiGateway"
  description = "API Gateway Serving Solo Stylist App"
}
