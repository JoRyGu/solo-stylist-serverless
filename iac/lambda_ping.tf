module "lambda_ping" {
  source               = "./modules/lambda"
  lambda_name          = "ping"
  artifact_name        = "../artifacts/lambdas/ping.zip"
  handler              = "main"
  api_auth             = "CUSTOM"
  api_gateway_id       = local.api_gw_id
  api_gateway_exec_arn = local.api_gw_exec_arn
  http_method          = "GET"
  parent_resource_id   = local.api_gw_root_resource_id
  resource_path        = "ping"
  authorizer_id        = aws_api_gateway_authorizer.authorizer.id
}
