#!/bin/bash

build () {
  (
    rm -rf ./artifacts
    mkdir -p ./artifacts/lambdas
    cd lambdas
    for dir in */; do
      (
        funcName="${dir/\/}"
        cd "$dir"
        GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o main
        zip "${funcName}.zip" main
        rm main
        mv "${funcName}.zip" ../../artifacts/lambdas/"${funcName}".zip
      )
    done
  )
}

deploy() {
  (
    cd iac

    terraform init
    terraform plan
    terraform apply -auto-approve
  )
}

build
deploy
