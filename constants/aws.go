package constants

const PolicyVersion = "2012-10-17"
const EffectAllow = "Allow"
const EffectDeny = "Deny"
const AuthorizerAction = "execute-api:Invoke"
