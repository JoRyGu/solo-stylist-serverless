job("Build and Deploy") {
    container(displayName = "Solo Stylist Serverless Pipeline", image = "golang") {
        env["AWS_ACCESS_KEY_ID"] = Secrets("aws_access_key_id")
        env["AWS_SECRET_ACCESS_KEY"] = Secrets("aws_secret_access_key")
        env["AWS_REGION"] = "us-east-1"

        shellScript {
            interpreter = "/bin/sh"
            content = """
               apt-get update && apt-get install -y lsb-release && apt-get clean all

               apt-get -y install zip

               wget -O- https://apt.releases.hashicorp.com/gpg | \
                   gpg --dearmor | \
                   tee /usr/share/keyrings/hashicorp-archive-keyring.gpg

               gpg --no-default-keyring \
    --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
    --fingerprint

               echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
    https://apt.releases.hashicorp.com ${'$'}(lsb_release -cs) main" | \
    tee /etc/apt/sources.list.d/hashicorp.list

                apt-get update
                
                apt-get install terraform

                ./cicd.sh
            """
        }
    }
}