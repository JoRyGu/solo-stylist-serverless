package main

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func Handler(_, event events.APIGatewayCustomAuthorizerRequest) (events.APIGatewayCustomAuthorizerResponse, error) {
	if event.Type != "TOKEN" || event.AuthorizationToken != "testing123" {
		return MakeFailurePolicy()
	}

	return MakeSuccessPolicy(event.AuthorizationToken)
}

func main() {
	lambda.Start(Handler)
}
