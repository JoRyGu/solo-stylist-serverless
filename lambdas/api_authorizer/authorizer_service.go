package main

import (
	"github.com/aws/aws-lambda-go/events"
	"solo_stylist_serverless/constants"
)

func MakeSuccessPolicy(principalId string) (events.APIGatewayCustomAuthorizerResponse, error) {
	return events.APIGatewayCustomAuthorizerResponse{
		PrincipalID: principalId,
		PolicyDocument: events.APIGatewayCustomAuthorizerPolicy{
			Version: constants.PolicyVersion,
			Statement: []events.IAMPolicyStatement{
				events.IAMPolicyStatement{
					Action: []string{constants.AuthorizerAction},
					Effect: constants.EffectAllow,
				},
			},
		},
	}, nil
}

func MakeFailurePolicy() (events.APIGatewayCustomAuthorizerResponse, error) {
	return events.APIGatewayCustomAuthorizerResponse{
		PolicyDocument: events.APIGatewayCustomAuthorizerPolicy{
			Version: constants.PolicyVersion,
			Statement: []events.IAMPolicyStatement{
				events.IAMPolicyStatement{
					Action: []string{constants.AuthorizerAction},
					Effect: constants.EffectDeny,
				},
			},
		},
	}, nil
}
